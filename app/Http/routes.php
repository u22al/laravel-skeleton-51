<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login','Auth\AuthController@getLogin');
Route::get('/register','Auth\AuthController@getRegister');
Route::get('/logout','Auth\AuthController@getLogout');
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::group(['middleware' => ['auth']], function(){
    Route::get('home', 'HomeController@index');
    Route::controllers([
        'setting' => 'User\SettingController'
    ]);
});

//resource.maker middleware should be disabled in production.
Route::group(['middleware' => ['resource.maker', 'auth.acl']], function(){
    Route::controllers([
        'user'  => 'User\UserController',
        'role'  => 'User\RoleController',
        'resource'  => 'User\ResourceController',
    ]);
});

