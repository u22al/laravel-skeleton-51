<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Routing\Route;
use Illuminate\Http\Response;
use App\Services\User\PermissionCheckService;

class AuthenticateWithAcl {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;
    
    /**
     *
     * @var Route
     */
    protected $route;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth, Route $route) {
        $this->auth = $auth;
        $this->route = $route;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {                    
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('auth/login');
            }
        }
        
        if(!PermissionCheckService::canAccess($this->route->getActionName(), $this->auth->user())){
            return new Response('Forbidden', 403);
        }

        return $next($request);
    }

}
