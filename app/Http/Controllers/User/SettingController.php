<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Services\User\SettingService;

class SettingController extends Controller {
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex() {
        $id = Auth::user()->user_id;
        return view('user.setting.index',[
            'user' => User::find($id)
        ]);                
    }
    
    public function postSaveProfile(Request $request, SettingService $settingService){
        $validator = $settingService->profileValidator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                    $request, $validator
            );
        }
        
        $settingService->saveProfile($request->all());
        
        return redirect()->back()->with('msg', 'Profile saved successfully!');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getChangePassword() {
        $id = Auth::user()->user_id;
        return view('user.setting.change-password',[
            'user' => User::find($id)
        ]);                
    }
    
    public function postSavePassword(Request $request, SettingService $settingService){
        $validator = $settingService->passwordValidator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                    $request, $validator
            );
        }
        
        $settingService->savePassword($request->only('password'));        
        return redirect()->back()->with('msg', 'Password changed successfully!');
    }

}
