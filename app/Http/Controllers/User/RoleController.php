<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User\Resource;
use App\Models\User\Role;
use App\Models\User\Permission;
use App\Services\User\RoleService;

class RoleController extends Controller {
        
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */        
    public function getIndex(){                
        return view('user.role.index', [
            'rows' => Role::paginate(30)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate(RoleService $roleService) {
        
        return view('user.role.create',[
            'resources' => $roleService->groupResource(Resource::all())
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postStore(Request $request, RoleService $roleService) {
        $validator = $roleService->validator($request->all());

        if($validator->fails()){
            $this->throwValidationException($request, $validator);
        }
        
        $roleService->create($request->all());
        return redirect('/role')->with('msg', 'Role created successfully!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEdit($id, RoleService $roleService) {
        return view('user.role.edit',[
            'id' => $id,
            'role'=>Role::find($id),
            'resources' => $roleService->groupResource(Resource::all()),
            'permissions'=>  $roleService->getPermissionArray(Permission::role($id)->get())
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postUpdate($id, RoleService $roleService, Request $request) {
        $validator = $roleService->validator($request->all(), $id);

        if($validator->fails()){
            $this->throwValidationException($request, $validator);
        }
        
        $roleService->update($id, $request->all());
        return redirect('/role')->with('msg', 'Role updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function getDestroy($id) {
        Role::destroy($id);
        Permission::where('role_id','=',$id)->delete();
        return redirect('/role')->with('msg', 'Role removed successfully!');
    }

}
