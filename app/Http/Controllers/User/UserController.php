<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User\User;
use App\Models\User\Role;
use App\Services\User\UserService;

class UserController extends Controller {
        
    public function getIndex() {
        return view('user.user.index', [
            'rows' => User::paginate(30)
        ]);
    }

    public function getCreate() {
        return view('user.user.create', [
            'roles' => Role::all()
        ]);
    }

    public function postStore(Request $request, UserService $service) {
        $validator = $service->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                    $request, $validator
            );
        }
        
        $service->create($request->all());
        
        return redirect('/user')->with('msg', 'User created successfully!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEdit($id) {
        return view('user.user.edit',[
            'id'=>$id,
            'roles' => Role::all(),
            'user' => User::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postUpdate($id, Request $request, UserService $service) {
        $validator = $service->editValidator($request->all(), $id);

        if ($validator->fails()) {
            $this->throwValidationException(
                    $request, $validator
            );
        }
                
        $service->update($request->all(), $id);
        
        return redirect('/user')->with('msg', 'User updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function getDestroy($id) {
        User::destroy($id);
        return redirect('/user')->with('msg', 'User deleted successfully!');
    }
    
    /**
     * 
     * @param int $id
     * @param string $state
     * @return Response
     */
    public function getActivate($id, $state) {
        $row = User::find($id);
        if ($state == 'yes') {
            $row->is_active = true;
            $status = 'activated';
        } else {
            $row->is_active = false;
            $status = 'de-activated';
        }

        $row->save();
        return redirect()->back()->with('msg', 'User ' . $status . ' successfully!');
    }

}
