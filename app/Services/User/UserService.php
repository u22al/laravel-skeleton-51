<?php

namespace App\Services\User;

use Validator;
use App\Models\User\User;

/**
 * Description of UserService
 *
 * @author A N M Mahabubul Hasan <uzzal@outlook.com>
 */
class UserService {

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function create(array $data) {
        return User::create([
                    'role_id' => 2,
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'is_active' => false
        ]);
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function editValidator(array $data, $id) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users,email,'.$id.',user_id',
                    'role_id' => 'exists:roles'
        ]);
    }

    /**
     *
     * @param array $data
     * @param int $id
     * @return User
     */
    public function update(array $data, $id){
        $user = User::find($id);
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->role_id = $data['role_id'];
        $user->save();

        return $user;
    }

}
