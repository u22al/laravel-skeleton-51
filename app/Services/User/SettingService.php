<?php
namespace App\Services\User;

use Validator;
use App\User;
use Auth;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

/**
 *
 * @author Mahabubul Hasan Uzzal <mahabubul.hasan@dnet.org.bd>
 */
class SettingService {
    protected $hasher;
    
    public function __construct(HasherContract $hasher) {
        $this->hasher = $hasher;
    }

    /**
     *
     * @param  array  $data
     * @param int $id
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function profileValidator(array $data) {
        $id = Auth::user()->user_id;
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$id.',user_id',
        ]);
    }
    
    /**
     *
     * @param  array  $data
     * @param int $id
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function passwordValidator(array $data) {
        Validator::extend('valid', function($attribute, $value, $parameters)
        {
            $user = User::find(Auth::user()->user_id);
            return $this->hasher->check($value, $user->password);
        });
        
        return Validator::make($data, [
            'current_password' => 'required|min:6|valid',
            'password' => 'required|confirmed|min:6',
            ],
            ['current_password.valid' => 'The current password is not valid.']
        );
    }
    
    public function savePassword($data){
        $user = User::find(Auth::user()->user_id);
        $user->password = bcrypt($data['password']);
        $user->save();
        
        return $user;
    }
    
    public function saveProfile($data){
        $user = User::find(Auth::user()->user_id);
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->save();
        
        return $user;
    }

}
