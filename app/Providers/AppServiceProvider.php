<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('nullsafe', function($expression) {
            return self::_nullsafeParser($expression);
        });
    }
    
    /**
     * @author uzzal
     */
    private static function _nullsafeParser($str, $pre = '') {
        $head = substr($str, 0, strpos($str, '->'));
        $tail = substr($str, strlen($head) + 2);

        if (strpos($tail, '->') == 0) {
            return '(' . $pre . $head . ')?' . $pre . $head . '->' . $tail . ':\'\')';
        } else {
            return '(' . $pre . $head . ')?' . self::_nullsafeParser($tail, $pre . $head . '->') . ':\'\')';
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
