<?php

use Illuminate\Database\Seeder;
use App\Models\User\Resource;

class ResourceTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {                
        Resource::create(['name'=>'User-User Index', 'controller'=>'User-User', 'action'=>'App\Http\Controllers\User\UserController@getIndex']);
        Resource::create(['name'=>'User-User Create', 'controller'=>'User-User', 'action'=>'App\Http\Controllers\User\UserController@getCreate']);
        Resource::create(['name'=>'User-User Edit', 'controller'=>'User-User', 'action'=>'App\Http\Controllers\User\UserController@getEdit']);
        Resource::create(['name'=>'User-User Destroy', 'controller'=>'User-User', 'action'=>'App\Http\Controllers\User\UserController@getDestroy']);
        Resource::create(['name'=>'User-User Store', 'controller'=>'User-User', 'action'=>'App\Http\Controllers\User\UserController@postStore']);
        Resource::create(['name'=>'User-User Update', 'controller'=>'User-User', 'action'=>'App\Http\Controllers\User\UserController@postUpdate']);
        Resource::create(['name'=>'User-User Activate', 'controller'=>'User-User', 'action'=>'App\Http\Controllers\User\UserController@getActivate']);
        
        Resource::create(['name'=>'User-Role Index', 'controller'=>'User-Role', 'action'=>'App\Http\Controllers\User\RoleController@getIndex']);
        Resource::create(['name'=>'User-Role Create', 'controller'=>'User-Role', 'action'=>'App\Http\Controllers\User\RoleController@getCreate']);
        Resource::create(['name'=>'User-Role Edit', 'controller'=>'User-Role', 'action'=>'App\Http\Controllers\User\RoleController@getEdit']);
        Resource::create(['name'=>'User-Role Destroy', 'controller'=>'User-Role', 'action'=>'App\Http\Controllers\User\RoleController@getDestroy']);
        Resource::create(['name'=>'User-Role Store', 'controller'=>'User-Role', 'action'=>'App\Http\Controllers\User\RoleController@postStore']);
        Resource::create(['name'=>'User-Role Update', 'controller'=>'User-Role', 'action'=>'App\Http\Controllers\User\RoleController@postUpdate']);
        
        Resource::create(['name'=>'User-Resource Index', 'controller'=>'User-Resource', 'action'=>'App\Http\Controllers\User\ResourceController@getIndex']);
        Resource::create(['name'=>'User-Resource Create', 'controller'=>'User-Resource', 'action'=>'App\Http\Controllers\User\ResourceController@getCreate']);
        Resource::create(['name'=>'User-Resource Edit', 'controller'=>'User-Resource', 'action'=>'App\Http\Controllers\User\ResourceController@getEdit']);
        Resource::create(['name'=>'User-Resource Destroy', 'controller'=>'User-Resource', 'action'=>'App\Http\Controllers\User\ResourceController@getDestroy']);
        Resource::create(['name'=>'User-Resource Store', 'controller'=>'User-Resource', 'action'=>'App\Http\Controllers\User\ResourceController@postStore']);
        Resource::create(['name'=>'User-Resource Update', 'controller'=>'User-Resource', 'action'=>'App\Http\Controllers\User\ResourceController@postUpdate']);
    }

}
