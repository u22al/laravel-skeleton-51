@extends('app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/user">User</a></li>
            <li class="active">List</li>
        </ol>
        @if($msg = session('msg'))
        <div class="alert alert-success" role="alert">{{ $msg }}</div>
        @endif
        <div class="col-md-12">            
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-left">User List</div>
                    <div class="pull-right"><a class="btn-link" href="/user/create">Create New</a></div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Controller</th>
                                    <th>Role</th>
                                    <th style="width:150px">Operation</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $can_edit = has_access('User\UserController@getEdit');
                                $can_del = has_access('User\UserController@getDestroy');  
                                $can_activate = has_access('User\UserController@getActivate');
                                $index = row_serial_start($rows);
                                ?>                                
                                @forelse($rows as $r)
                                <tr>
                                    <td>{{$index++}}.</td>
                                    <td>{{ $r->name }}</td>
                                    <td>{{ $r->email }}</td>
                                    <td>{{ $r->role->name }}</td>
                                    <td>
                                        @if($can_edit)
                                        <a href="{{url('/user/edit',['id' => $r->user_id ])}}">Edit</a>
                                        @endif
                                        @if($can_activate)
                                            @if($r->is_active)
                                            <a onclick="return confirm('Are you sure you want to deactivate?')" href="{{url('/user/activate',['id' => $r->user_id, 'state'=>'no'])}}">Deactivate</a>
                                            @else
                                            <strong><a href="{{url('/user/activate',['id' => $r->user_id,'state'=>'yes'])}}">Activate</a></strong>
                                            @endif
                                        @endif                                        
                                        @if($can_del && $can_activate || $can_edit && $can_del)
                                        /
                                        @endif
                                        @if($can_del)
                                        <a onclick="return confirm('Are you sure you want to delete?')" href="{{url('/user/destroy',['id' => $r->user_id ])}}">Del</a>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                <tr><td colspan="5">No data found!</td></tr>
                                @endforelse                               
                            </tbody>
                        </table>
                    </div>
                    <div>{!! $rows->render() !!}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


